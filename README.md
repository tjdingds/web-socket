# web-socket

#### 介绍
websocket测试

#### 软件架构
后端 net core 3.1 webapi
前端 chrome F12 
 console页
 var wsobj = {
	init:function(){
	   var ws = new WebSocket("wss://localhost:5001/ws");
           ws.onmessage = function(data){console.log(data)};
	   ws.onopen = function(data){console.log("open")};
           ws.onclose = function(data){console.log("close")};
	   return ws;
	}
}

var ws = wsobj.init();
ws.send("Hello")

关于 core 3.1 ，调试模式为：1、IISExpress  2、托管运行模式
  IIS Express 运行，本地在 控制面板->程序->添加和删除服务 中 安装 web socket协议
如果core 项目是Https 启动，
则 url 格式： wss:// ，否则用 ws://

###源码来源
https://mp.weixin.qq.com/s/EzGbotrw6cx_DD7n0ZMbNA

 

